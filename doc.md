---
title: HyNgine
author: A. Kuhn, S. Mellace, C. Paoliello
date: Jaunary 2018
institute: SUPSI-DTI

toc: true
---

# Documentation for HyNgine engine

## Introduction

The aim of this project was to implement a graphics engine, build on top of OpenGL, to allow a user to play a game where a helicopter needs to be guided from a start to a finish base without colliding with the terrain.

The game should be build in such a way as to show the correct implementation of the following elements in our engine:

* Rendering of vertices and normals
* Rendering of transparent materials
* Rendering of textures
* Rendering of reflections
* Rendering of lights
* Importing of scenes from the *OverVision Object (OVO)* file format
* Correct handling of the scene graph

On the client-side, we implemented the following:

* Loading of the scene
* Setting the moving object
* Initializing cameras
* Moving the helicopter blades
* Handling movement of the helicopter by keyboard
* Setting menu entries
* Handling collisions
* Handling gravity

We used 3ds Max to design the scene and export it in *OVO*. The code has been developed in C++ and it's been tested both under Windows and Linux operating systems.

## Structure

### Classes

The general class structure of our engine is shown in figure \ref{class-diagram}.

![Class diagram of HyNgine\label{class-diagram}](img/classes.png)

As you can see our engine has a list of Object, by which we can simply render the entire scene graph. The Mesh class can have a Material and if it is present, this Material can also have a Texture.
To the standard model classes we added a Triangle class. This class is useful for debug purposes to render a simple triangle on a scene.

### Engine

Our engine has a *static* `getInstance` function that acts as a singleton and allows us to access the engine instance from anywhere in the code (both from the client and backend-side). The first time it is called, the only instance of the engine is created.

## Methodology

We almost always *pair programmed* in class, to allow exchange of ideas and different solutions to the problems we encountered. Some parts, especially in the late development phase, have been programmed indidually after assigning to each of us equal amounts of work.

## Algorithms

### Efficient world coordinates computation

In our implementation each Node of the scene graph has a `content` and a `finalMatrix`. The first one is the position of the node relative to its father's position. The second one is a matrix containing the position of the node expressed in world coordinates. This allows us to easily get the position in world coordinates without the need to traverse the tree everytime we need to render the Node.

The `finalMatrix` is thus only updated when the client calls the `setContent` function, and then recursively calls the `updateFinalMatrix` function on each of the children in the subtree, each time passing the current `finalMatrix` as an argument so that each child needs to perform only one matrix multiplication. This is especially useful in our case, since only the helicopter (and its components, accordingly) will ever change position. Every other node will only need to traverse the tree once when launching the application.

```cpp
void Node::setContent(mat4 &content)
{
   m_content = content;
   if(m_father != nullptr){
      mat4 i = m_father->getFinalMatrix();
      updateFinalMatrix(&i);
   }else {
      mat4 i = mat4(1.f);
      updateFinalMatrix(&i);
   }
}

void Node::updateFinalMatrix(mat4 *base)
{
   m_finalMatrix = (*base) * m_content;
   for (Node* n : m_children)
      n->updateFinalMatrix(&m_finalMatrix);
}
```

### Sorting and rendering of object list

The List contains an ordered set pointers to Objects. When it is sorted, it is done according to the type of Object, distinguishing three of those types. The first type to be rendered are Lights, then opaque objects and then every other object including the ones with transparency.

### Reflections

Reflections can be activated by the user through the `setReflections` functions.

To correctly render the reflection of the scene, the sorted list is first rendered by passing a *modifier* matrix to the render method: a symmetry matrix

$$S = \begin{pmatrix}1 & 0 & 0 \\ 0 & -1 & 0 \\ 0 & 0 & 1\end{pmatrix}$$

Some nodes ignore this modifier, for example lights and meshes with transparency, since the double rendering is to be avoided in these cases.

Only after this first render, the original list is rendered, without any *modifier*.

This following code snippet is taken from the `displayCallback` function and illustrates this algorithms:

```cpp
// Render reflected scene, if reflection set
if (HyNgine::getInstance()->m_reflections == true)
{
  mat4 scale = glm::scale(mat4(1.f), vec3(1.f, -1.f, 1.f));
  HyNgine::getInstance()->m_list.render(&scale);
}

// Render scene
HyNgine::getInstance()->m_list.render();
```

### Sorting of vertices with transparency

Transparent object also need a sort for each vertex so that we can render closer objects last. This sorting is made by the `sortTriangles` method of Mesh: it computes the distance between the position of the camera and the center of each triplet of vertices, then orders those "triangles" by distance (longest distance is first).
An in-place algorithm alternative has been considered for implentation, but since our scene doesn't have that many transparent triangles, "performance loss" (a couple of fps in *Debug* mode) due to list copying isn't an issue and we favoured the more readable and straight-forward algorithm.

Since the eye coordinates of the triangles can change each frame this method must be efficent, this is the reason why we used a quicksort but not directly on the vertices. We have to sort in the same way also the array of normals and the coordinates of the texture (if present), in order to do so we prepared an array containing the initial permutation of the triangles, that is, an array of ordered numbers from 0 to the number of vertices divided by three. Then we sort the permutation array by doing the comparisons on consecutive triplets from the vertices array. The result is that the permutation array will now contain the indices corresponding to the sorted vertices array. Afterwards we simply swap the elements of the vertices, normal and texture coordinates arrays according to such indices.

## Issues & Improvements

### Spotlights

The spotlight on the helicopter has correct direction and renders just fine in third person view, yet when the camera is positioned on the helicopter, the direction doesn't always appear to be correct (for example when rotating the helicopter).

### Collision handling

Collisions are quite expensive in the current implementation, since we check for **every** vertex of the entire scene if its distance is too close to the helicopter.

## Conclusions

Our engine is able to render a scene of 1042 vertices at an average of 700 fps (in *Release* mode in a native OS).

We almost entirely reached the initial goals of this project, except for the issue regarding the rendering of spotlights. 

A screenshot of our final project can be seen in figure \ref{finalScene}

![A screenshot of the game rendered using HyNgine\label{finalScene}](img/finalScene.jpg)