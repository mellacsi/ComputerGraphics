var indexSectionsWithContent =
{
  0: "bcdfghilmnoprstu~",
  1: "chlmnot",
  2: "chlmnot",
  3: "bcdfgimnoprsu~",
  4: "op",
  5: "s",
  6: "d",
  7: "l"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "enums",
  5: "enumvalues",
  6: "related",
  7: "defines"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Files",
  3: "Functions",
  4: "Enumerations",
  5: "Enumerator",
  6: "Friends",
  7: "Macros"
};

