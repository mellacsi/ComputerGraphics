// HyNgine
#include "Node.h"
#include <list>
#include <queue>
#include "HyNgine.h"
#include "Camera.h"

/**
* Get the final matrix which takes into account transformations of
* the father node.
* @return the final matrix of the node
*/
mat4 LIB_API Node::getFinalMatrix()
{
   return m_finalMatrix;
}

/**
* Set the content of the Node and update the final matrix.
* @param[in] content is the new content of the node.
*/
void Node::setContent(mat4 & content)
{
   m_content = content;
   if(m_father != nullptr){
      mat4 i = m_father->getFinalMatrix();
      updateFinalMatrix(&i);
   }else {
      mat4 i = mat4(1.f);
      updateFinalMatrix(&i);
   }

}

/**
* Remove given child.
* @param[in] node is the node to remove.
*/
void Node::removeChild(Node * node)
{
   int idx = 0;
   for (Node *n : m_children)
   {
      if (n == node)
         break;
      idx++;
   }

   if (idx < m_children.size())
      m_children.erase(m_children.begin() + idx);
}

/**
* Find a Node by name.
* @param[in] name is the name of the searched node.
*
* @return found Object or nullptr
*/
Object * Node::findByName(string name)
{
   Node *result = nullptr;

   queue<Node*> q;
   q.push(this);

   while (!q.empty())
   {
      Node *current = q.front();
      q.pop();
      for (Node *n : current->getChildren())
         q.push(n);
      if (current->getName() == name) {
         result = current;
         break;
      }
   }

   return result;
}

/**
* Update the final matrix of the Node and his children.
* @param[in] base is the matrix of the father, so that you can recursively
* calculate the new final matrix of the children.
*/
void Node::updateFinalMatrix(mat4 * base)
{
   m_finalMatrix = (*base) * m_content;
   for (Node* n : m_children)
      n->updateFinalMatrix(&m_finalMatrix);
}

/*
* Copy contructor
*@param[in] n is the node to clone.
*/
Node::Node(const Node & n)
   : m_content{ n.m_content },
   m_finalMatrix{ n.m_finalMatrix },
   m_children{ n.m_children.begin(), n.m_children.end() },
   m_father{ n.m_father }
{
}

/*
* Assignement operator
*@param[in] n is the node to assign.
*/
Node & Node::operator=(const Node & n)
{
   m_content = n.m_content;
   m_finalMatrix = n.m_finalMatrix;
   m_children = vector<Node*>(n.m_children.begin(), n.m_children.end());
   m_father = n.m_father;
   return *this;
}

/**
* Constructor.
*/
Node::Node()
{
}
