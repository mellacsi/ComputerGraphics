/// HyNgine
#include "HyNgine.h"
#include "Triangle.h"
#include "Light.h"
/// C++
#include <iostream>
#include <list>
using namespace std;
/// FreeGLUT
#include <GL/freeglut.h>
/// FreeImage
#include "FreeImage.h"
/// GLM
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/packing.hpp>
#include <glm/gtx/string_cast.hpp>

/*! \def  OV_MAXNUMBEROFCHARS
\brief Ovo reader limits
*/
#define OV_MAXNUMBEROFCHARS   256

/*! \class  OvObject
\brief Enum class for reading ovo purpose.
*/
typedef int32_t	ovInt;
class OvObject
{
public:
	enum class Type:ovInt
	{
		// Foundation types:
		OBJECT = 0,
		NODE,
		OBJECT2D,
		OBJECT3D,
		LIST,

		// Derived classes:
		BUFFER,
		SHADER,
		TEXTURE,
		FILTER,
		MATERIAL,
		FBO,
		QUAD,
		BOX,
		SKYBOX,
		FONT,
		CAMERA,
		LIGHT,
		BONE,
		MESH,	   // Keep them...
		SKINNED,  // ...consecutive                 
		PIPELINE,
		EMITTER,

		// Animation type
		ANIM,

		// Physics related:
		PHYSICS,

		// Terminator:
		LAST,
	};
};
/**
* Definitions of type for ovo reader.
*/
typedef uint8_t            ovByte;
typedef float					ovFloat;
typedef void					ovVoid;
typedef uint32_t				ovDWord;

/*! \class  OvPhysics
\brief class for reading ovo purpose.
*/
class  OvPhysics
{
public:
struct ovProps
{
	enum : ovByte ///< Kind of physical objects
	{
		PHYS_UNDEFINED = 0,
		PHYS_DYNAMIC = 1,
		PHYS_KINEMATIC = 2,
		PHYS_STATIC = 3,
		PHYS_LAST
	};

	enum : ovByte ///< Kind of hull
	{
		HULL_UNDEFINED = 0,
		HULL_SPHERE = 1,
		HULL_BOX = 2,
		HULL_CAPSULE = 3,
		HULL_CONVEX = 4,
		HULL_ORIGINAL = 5,
		HULL_CUSTOM = 6,
		HULL_CONCAVE = 7,
		HULL_LAST
	};

	// Pay attention to 16 byte alignement (use padding):      
	ovByte type;
	ovByte contCollisionDetection;
	ovByte collideWithRBodies;
	ovByte hullType;

	// Vector data:
	vec3 massCenter;

	// Mesh properties:
	ovFloat mass;
	ovFloat staticFriction;
	ovFloat dynamicFriction;
	ovFloat bounciness;
	ovFloat linearDamping;
	ovFloat angularDamping;
	ovVoid *physObj;
	//ovFloat _pad1;    
	//ovFloat _pad2; 
};
};

using namespace glm;

HyNgine *instance = nullptr; /*!< HyNgine instance, it is singleton */

int frames = 0; /*!< Frame counter */
float fps = 0.0f; /*!< fps */

/**
* Populate the list with the scene graph and enter glut main loop. Free resources before returning.
*
* @return 0 if succeeded
*/
void LIB_API HyNgine::start()
{
   // Define final matrices of tree nodes
   mat4 i = mat4(1.f);
   HyNgine::getInstance()->getSceneGraph()->updateFinalMatrix(&i);

	HyNgine::getInstance()->m_list.populate(HyNgine::getInstance()->m_sceneGraph);
	HyNgine::getInstance()->m_list.sort();

   // Debug: check if list is ordered correctly
   for (Object *o : HyNgine::getInstance()->m_list.getList())
      cout << "<<<" << o->getName() << endl;

	glutMainLoop();
}

/**
* This method is useful to set up the content of the menu.
* @param[in] argc number of elements
* @param[in] argv elements to see
*/
void LIB_API HyNgine::setMenu(int argc, vector<string> argv)
{
   m_argc = argc;
	m_argv = argv;
}

/**
* Free the scene graph.
* @param[in] root is the scene graph.
*/
void HyNgine::freeScene(Node * root)
{
   if (root->getNumberOfChildren() > 0)
      for (Node * n : root->getChildren())
         freeScene(n);
   delete root;
}

/**
* Free the memory (free the tree, textures, maps,...).
*/
void LIB_API HyNgine::freeContext()
{
   HyNgine *engine = HyNgine::getInstance();

   // Free scene graph
   freeScene(engine->m_sceneGraph);
   cout << "Scene graph Freed" << endl;

   // Free map of Materials
   for (std::map<string, Material *>::iterator it = engine->m_materialMap.begin(); it != engine->m_materialMap.end(); ++it) 
   {
      if (it->second->getTexture().getFileName() != "[none]") {
         glDeleteTextures(1, it->second->getTexture().getTexId()); /*!< Delete texture */
      }
      delete it->second; /*!< Delete material */
   }

   engine->m_materialMap.clear();
   cout << "Material and textures freed" << endl;
   
   // Free the list
   engine->m_list.clear();
   cout << "List Freed" << endl;

   // Free engine instance
   delete instance;

   FreeImage_DeInitialise();

   // Done:
   std::cout << "[application terminated]" << std::endl;
}

/**
* Factory method which calls the triangle constructor.
* @param[in] edge is the triangle's edge dimension.
*
* @return a new Triangle
*/
Triangle *HyNgine::genTriangle(const float & edge)
{
	return new Triangle(edge);
}

/**
* Destruct a Triangle.
* @param[in] triangle to destroy.
*/
void HyNgine::destructTriangle(Triangle *triangle)
{
   delete triangle;
}

/**
* Create new light with default values.
*
* @return a new Light
*/
Light *HyNgine::genLight()
{
	return new Light();
}

/**
* This callback is invoked each second.
* @param[in] value passepartout value
*/
void timerCallback(int value)
{
	fps = frames / 1.0f;
	frames = 0;

	// Register the next update:
	glutTimerFunc(1000, timerCallback, 0);
}

/**
* This is the main rendering routine automatically invoked by FreeGLUT.
*
*/
void displayCallback()
{
   /// Prepare and set up right matrix mode, buffers,...
	glDepthMask(GL_TRUE);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glMatrixMode(GL_PROJECTION);
	glLoadMatrixf(glm::value_ptr(HyNgine::getInstance()->getPerspective()));
	glMatrixMode(GL_MODELVIEW);

   /// Render reflected scene, if reflection set
   if (HyNgine::getInstance()->m_reflections == true)
   {
      mat4 scale = glm::scale(mat4(1.f), vec3(1.f, -1.f, 1.f));
      HyNgine::getInstance()->m_list.render(&scale);
   }

   /// Render scene
   HyNgine::getInstance()->m_list.render();

   ///Show commands tips
	glMatrixMode(GL_PROJECTION);
	   glLoadMatrixf(glm::value_ptr(HyNgine::getInstance()->getOrtho()));
	glMatrixMode(GL_MODELVIEW);
	glLoadMatrixf(glm::value_ptr(glm::mat4(1.0)));

	glDisable(GL_LIGHTING);
	char text[64];
   for (int i = 0; i < HyNgine::getInstance()->m_argc; i++)
   {
      sprintf(text, HyNgine::getInstance()->m_argv[i].c_str());
      glRasterPos2f(2.0f, (HyNgine::getInstance()->m_argc - i )* 13.f + 20.f);
      glutBitmapString(GLUT_BITMAP_8_BY_13, (unsigned char *)text);
   }

   sprintf(text, "FPS: %.1f", fps);

   glRasterPos2f(2.f , glutGet(GLUT_WINDOW_HEIGHT) - 15.f);
   glutBitmapString(GLUT_BITMAP_8_BY_13, (unsigned char *)text);

	glutSwapBuffers();

	glutPostWindowRedisplay(HyNgine::getInstance()->windowId);

	// Inc. frames counter:
	frames++;
   glEnable(GL_LIGHTING);
}

/**
* This callback is invoked each time the window gets resized (and once also when created).
* @param[in] width new window width
* @param[in] height new window height
*/
void reshapeCallback(int width, int height)
{
	glViewport(0, 0, width, height);
	Camera *camera = HyNgine::getInstance()->getCamera();

	HyNgine::getInstance()->setPerspective(glm::perspective(glm::radians(camera->getFovy()), (float)width / (float)height, camera->getNearPlane(), camera->getFarPlane()));
	HyNgine::getInstance()->setOrtho(glm::ortho(0.0f, (float)width, 0.0f, (float)height, -1.0f, 1.0f));
}

/**
 * Initialize OpenGL context
 * @param[in] argc number of elements received
 * @param[in] argv arguments received in an array
 */
void LIB_API HyNgine::initializeContext(int argc, char *argv[])
{
	glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH);
	glutInitWindowPosition(100, 100);
	glutInitWindowSize(800, 500);

	glutInit(&argc, argv);

	glutSetOption(GLUT_ACTION_ON_WINDOW_CLOSE, GLUT_ACTION_GLUTMAINLOOP_RETURNS);

	windowId = glutCreateWindow("CG Final Project");

	glutDisplayFunc(displayCallback);
	glutReshapeFunc(reshapeCallback);
	glutTimerFunc(3000, timerCallback, 0);

	glClearColor(1.f, 0.6f, 0.1f, 1.f);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_NORMALIZE);
	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHT1);
	glEnable(GL_CULL_FACE);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	// Default
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
	
   FreeImage_Initialise();
}

/**
* Load a chunk with his children from ovo file.
* In our case the chunk is a Node, a Mesh, or a Light, but it can read Bone too.
* @param[in] dat actual ptr to ovo file.
* @param[in] node it is the father node, the starting node.
*/
void HyNgine::loadChunk(FILE *dat, Node * node)
{
	unsigned int chunkId, chunkSize;
	fread(&chunkId, sizeof(unsigned int), 1, dat);
	fread(&chunkSize, sizeof(unsigned int), 1, dat);

   // Tmp data
   vector<vec3> tmpVertices;
   vector<vec4> tmpNormals;
   vector<vec2> tmpTextureCoords;

	// Load whole chunk into memory:
	char *data = new char[chunkSize];
	if (fread(data, sizeof(char), chunkSize, dat) != chunkSize)
	{
		fclose(dat);
		delete[] data;
		return;
	}

	// Parse chunk information according to its type:
	unsigned int position = 0;
	switch ((OvObject::Type) chunkId)
	{
		/////////////////////////////
		case OvObject::Type::NODE: //
		{
			// Node name:
			char nodeName[OV_MAXNUMBEROFCHARS];
			strcpy(nodeName, data + position);
			position += (unsigned int)strlen(nodeName) + 1;

			// Node matrix:
			mat4 matrix;
			memcpy(&matrix, data + position, sizeof(mat4));
			position += sizeof(mat4);

			// Nr. of children nodes:
			unsigned int children;
			memcpy(&children, data + position, sizeof(unsigned int));
			position += sizeof(unsigned int);

			// Optional target node, [none] if not used:
			char targetName[OV_MAXNUMBEROFCHARS];
			strcpy(targetName, data + position);
			position += (unsigned int)strlen(targetName) + 1;

			Node* tmpNode = new Node(node);
			m_idCounter++;
			tmpNode->setName(nodeName);
			tmpNode->setContent(matrix);
			node->addChild(tmpNode);
         cout << node->getName() << "->" << tmpNode->getName() << endl;

			for(int i = 0; i < children; i++)
				loadChunk(dat, tmpNode);
		}
		break;

		////////////////////////////////
		case OvObject::Type::MESH:    //
		case OvObject::Type::SKINNED:
		{
			Mesh *tmpMesh = new Mesh(node);

			// Both standard and skinned meshes are handled through this case:
			bool isSkinned = false;
			if ((OvObject::Type) chunkId == OvObject::Type::SKINNED)
			{
				isSkinned = true;
			}

			// Mesh name:
			char meshName[OV_MAXNUMBEROFCHARS];
			strcpy(meshName, data + position);
			position += (unsigned int)strlen(meshName) + 1;
			
         tmpMesh->setName(meshName);

			// Mesh matrix:
			mat4 matrix;
			memcpy(&matrix, data + position, sizeof(mat4));
			position += sizeof(mat4);

         tmpMesh->setContent(matrix);

			// Mesh nr. of children nodes:
			unsigned int children;
			memcpy(&children, data + position, sizeof(unsigned int));
			position += sizeof(unsigned int);

			// Optional target node, or [none] if not used:
			char targetName[OV_MAXNUMBEROFCHARS];
			strcpy(targetName, data + position);
			position += (unsigned int)strlen(targetName) + 1;

			// Mesh subtype (see OvMesh SUBTYPE enum):
			unsigned char subtype;
			memcpy(&subtype, data + position, sizeof(unsigned char));
			position += sizeof(unsigned char);

			// Nr. of vertices:
			unsigned int vertices, faces;
			memcpy(&vertices, data + position, sizeof(unsigned int));
			position += sizeof(unsigned int);

			// ...and faces:
			memcpy(&faces, data + position, sizeof(unsigned int));
			position += sizeof(unsigned int);

			// Material name, or [none] if not used:
			char materialName[OV_MAXNUMBEROFCHARS];
			strcpy(materialName, data + position);
			position += (unsigned int)strlen(materialName) + 1;

         // Add material to mesh
         // CHECK: if deep copy of material is done
         if (materialName != "[none]")
            tmpMesh->setMaterial(*m_materialMap[string(materialName)]);

			// Mesh bounding sphere radius:
			float radius;
			memcpy(&radius, data + position, sizeof(float));
			position += sizeof(float);
			tmpMesh->setSphereRadius(radius);

			// Mesh bounding box minimum corner:
			vec3 bBoxMin;
			memcpy(&bBoxMin, data + position, sizeof(vec3));
			position += sizeof(vec3);

			// Mesh bounding box maximum corner:
			vec3 bBoxMax;
			memcpy(&bBoxMax, data + position, sizeof(vec3));
			position += sizeof(vec3);

			// Optional physics properties:
			unsigned char hasPhysics;
			memcpy(&hasPhysics, data + position, sizeof(unsigned char));
			position += sizeof(unsigned char);
			if (hasPhysics)
			{
				position += sizeof(OvPhysics::ovProps);
			}

			// Interleaved and compressed vertex/normal/UV/tangent data:                    
			for (unsigned int c = 0; c < vertices; c++)
			{
				// Vertex coords:    
				vec3 vertex;
				memcpy(&vertex, data + position, sizeof(vec3));
				position += sizeof(vec3);

            tmpVertices.push_back(vertex);

				// Vertex normal:
				unsigned int normalData;
				memcpy(&normalData, data + position, sizeof(unsigned int));
				vec4 normal = unpackSnorm3x10_1x2(normalData);
				position += sizeof(unsigned int);

            tmpNormals.push_back(normal);

				// Texture coordinates:
				unsigned short textureData[2];
				memcpy(textureData, data + position, sizeof(unsigned short) * 2);
				vec2 uv;
				uv.x = unpackHalf1x16(textureData[0]);
				uv.y = unpackHalf1x16(textureData[1]);
				position += sizeof(unsigned short) * 2;

            tmpTextureCoords.push_back(uv);

				// Tangent vector:
				unsigned int tangentData;
				memcpy(&tangentData, data + position, sizeof(unsigned int));
				vec4 tangent = unpackSnorm3x10_1x2(tangentData);
				position += sizeof(unsigned int);
			}

			// Faces:
			for (unsigned int c = 0; c < faces; c++)
			{
				// Face indexes:
				unsigned int face[3];
				memcpy(face, data + position, sizeof(unsigned int) * 3);
				position += sizeof(unsigned int) * 3;

            // Add vertex data to mesh
            for (int i = 0; i < 3; i++)
            {
               tmpMesh->addVertex(tmpVertices[face[i]]);
               tmpMesh->addNormal(tmpNormals[face[i]]);
               tmpMesh->addTextureCoords(tmpTextureCoords[face[i]]);
            }

			}

			// Extra information for skinned meshes:
			if (isSkinned)
			{
				// Initial mesh pose matrix:               
				position += sizeof(mat4);

				// Bone list:
				unsigned int nrOfBones;
				memcpy(&nrOfBones, data + position, sizeof(unsigned int));
				position += sizeof(unsigned int);

				for (ovDWord c = 0; c < nrOfBones; c++)
				{
					// Bone name:
					char boneName[OV_MAXNUMBEROFCHARS];
					strcpy(boneName, data + position);
					position += (unsigned int)strlen(boneName) + 1;

					// Initial bone pose matrix (already inverted):
					position += sizeof(mat4);
				}

				// Per vertex bone weights and indexes:               
				for (unsigned int c = 0; c < vertices; c++)
				{

					// Bone indexes:
					position += sizeof(unsigned int) * 4;

					// Bone weights:
					position += sizeof(unsigned short) * 4;
				}
			}

         node->addChild(tmpMesh);
         cout << node->getName() << "->" << tmpMesh->getName() << endl;
			m_idCounter++;
			for (int i = 0; i < children; i++)
				loadChunk(dat, tmpMesh);
		}
		break;


		//////////////////////////////
		case OvObject::Type::LIGHT: //
		{
			Light *tmpLight = new Light();

			// Light name:
			char lightName[OV_MAXNUMBEROFCHARS];
			strcpy(lightName, data + position);
			position += (unsigned int)strlen(lightName) + 1;
			tmpLight->setName(lightName);

			// Light matrix:
			mat4 matrix;
			memcpy(&matrix, data + position, sizeof(mat4));
			position += sizeof(mat4);
			tmpLight->setContent(matrix);

			// Nr. of children nodes:
			unsigned int children;
			memcpy(&children, data + position, sizeof(unsigned int));
			position += sizeof(unsigned int);

			// Optional target node name, or [none] if not used:
			char targetName[OV_MAXNUMBEROFCHARS];
			strcpy(targetName, data + position);
			position += (unsigned int)strlen(targetName) + 1;

			// Light subtype (see OvLight SUBTYPE enum):
			unsigned char subtype;
			memcpy(&subtype, data + position, sizeof(unsigned char));
			position += sizeof(unsigned char);
			tmpLight->setSubtype(subtype);

			// Light color:
			vec3 color;
			memcpy(&color, data + position, sizeof(vec3));
			position += sizeof(vec3);
			tmpLight->setColor(glm::vec4(color.r, color.g, color.b, 1.0f));

			// Influence radius:
			float radius;
			memcpy(&radius, data + position, sizeof(float));
			cout << "   Radius  . . . :  " << radius << endl;
			position += sizeof(float);

			// Direction:
			vec3 direction;
			memcpy(&direction, data + position, sizeof(vec3));
			cout << "   Direction . . :  " << direction.x << ", " << direction.y << ", " << direction.z << endl;
			position += sizeof(vec3);
			tmpLight->setLightDirection(glm::vec4(direction.x, direction.y, direction.z, 1.0f));

			// Cutoff:
			float cutoff;
			memcpy(&cutoff, data + position, sizeof(float));
			cout << "   Cutoff  . . . :  " << cutoff << endl;
			position += sizeof(float);
			tmpLight->setSpotAngle(cutoff);

			// Exponent:
			float spotExponent;
			memcpy(&spotExponent, data + position, sizeof(float));
			cout << "   Spot exponent :  " << spotExponent << endl;
			position += sizeof(float);
			
			node->addChild(tmpLight);
			cout << node->getName() << "->" << tmpLight->getName() << endl;
			m_idCounter++;
			tmpLight->setFather(node);
			for (int i = 0; i < children; i++)
				loadChunk(dat, tmpLight);

		}
		break;


		/////////////////////////////
		case OvObject::Type::BONE: //
		{
			cout << "bone]" << endl;

			// Bone name:
			char boneName[OV_MAXNUMBEROFCHARS];
			strcpy(boneName, data + position);
			position += (unsigned int)strlen(boneName) + 1;

			// Bone matrix:
			mat4 matrix;
			memcpy(&matrix, data + position, sizeof(mat4));
			position += sizeof(mat4);

			// Nr. of children nodes:
			unsigned int children;
			memcpy(&children, data + position, sizeof(unsigned int));
			position += sizeof(unsigned int);

			// Optional target node, or [none] if not used:
			char targetName[OV_MAXNUMBEROFCHARS];
			strcpy(targetName, data + position);
			position += (unsigned int)strlen(targetName) + 1;

			// Mesh bounding box minimum corner:
			vec3 bBoxMin;
			memcpy(&bBoxMin, data + position, sizeof(vec3));
			position += sizeof(vec3);

			// Mesh bounding box maximum corner:
			vec3 bBoxMax;
			memcpy(&bBoxMax, data + position, sizeof(vec3));
			position += sizeof(vec3);
		}
		break;


		///////////
		default: //
			cout << "UNKNOWN]" << endl;
			cout << "ERROR: corrupted chunk id " << chunkId << endl;
			fclose(dat);
			delete[] data;
	}
}

/*
* Load a hierarchy of materials, light and mesh from ovo file.
* Load materials into memory and call loadChunk method foreach NODE
* @param[in] fileName ovo's file name
* 
* @return the root of the scene graph Node
*/
Node LIB_API *HyNgine::loadMeshFromFile(string fileName)
{
   // Open file:
   FILE *dat = fopen(fileName.c_str(), "rb");
   if (dat == nullptr)
   {
      cout << "ERROR: unable to open file '" << fileName << "'" << endl;
      return nullptr;
   }

   // Configure stream:
   cout.precision(2);  // 2 decimals are enough
   cout << fixed;      // Avoid scientific notation

	Node* tmpNode = nullptr; //will be our root
   /////////////////
   // Parse chunks:	
   unsigned int chunkId, chunkSize;
   while (true)
   {
      fread(&chunkId, sizeof(unsigned int), 1, dat);
      if (feof(dat))
         break;
      fread(&chunkSize, sizeof(unsigned int), 1, dat);

      // Load whole chunk into memory:
      char *data = new char[chunkSize];
      if (fread(data, sizeof(char), chunkSize, dat) != chunkSize)
      {
         cout << "ERROR: unable to read from file '" << fileName << "'" << endl;
         fclose(dat);
         delete[] data;
         return nullptr;
      }
      // Parse chunk information according to its type:
      unsigned int position = 0;
      switch ((OvObject::Type) chunkId)
      {
      ///////////////////////////////
      case OvObject::Type::OBJECT: //
      {
         // OVO revision number:
         unsigned int versionId;
         memcpy(&versionId, data + position, sizeof(unsigned int));
         position += sizeof(unsigned int);
      }
      break;

      /////////////////////////////
      case OvObject::Type::NODE: //
      {
         // Node name:
         char nodeName[OV_MAXNUMBEROFCHARS];
         strcpy(nodeName, data + position);
         position += (unsigned int)strlen(nodeName) + 1;

         // Node matrix:
         mat4 matrix;
         memcpy(&matrix, data + position, sizeof(mat4));
         position += sizeof(mat4);

         // Nr. of children nodes:
         unsigned int children;
         memcpy(&children, data + position, sizeof(unsigned int));
         position += sizeof(unsigned int);

         // Optional target node, [none] if not used:
         char targetName[OV_MAXNUMBEROFCHARS];
         strcpy(targetName, data + position);
         position += (unsigned int)strlen(targetName) + 1;

			tmpNode = new Node(); // ROOT node
			m_idCounter++;
			tmpNode->setName(nodeName);
			tmpNode->setContent(matrix);
			for (int i = 0; i < children; i++)
				loadChunk(dat, tmpNode);
      }
      break;

      /////////////////////////////////
      case OvObject::Type::MATERIAL: //
		{
			Material *tmpMaterial = new Material();
			m_idCounter++;

			// Material name:
			char materialName[OV_MAXNUMBEROFCHARS];
			strcpy(materialName, data + position);
			position += (unsigned int)strlen(materialName) + 1;
			
         tmpMaterial->setName(materialName);

			// Material term colors, starting with emissive:
			vec3 emission, ambient, diffuse, specular;
			memcpy(&emission, data + position, sizeof(vec3));
			position += sizeof(vec3);
			
         tmpMaterial->setEmission(glm::vec4(emission.r, emission.g, emission.b, 1.0f));

			// Ambient:
			memcpy(&ambient, data + position, sizeof(vec3));
			position += sizeof(vec3);
			
         tmpMaterial->setAmbient(glm::vec4(ambient.r, ambient.g, ambient.b, 1.0f));

			// Diffuse:
			memcpy(&diffuse, data + position, sizeof(vec3));
			position += sizeof(vec3);
			
         tmpMaterial->setDiffuse(glm::vec4(diffuse.r, diffuse.g, diffuse.b, 1.0f));

			// Specular:
			memcpy(&specular, data + position, sizeof(vec3));
			position += sizeof(vec3);
			
         tmpMaterial->setSpecular(glm::vec4(specular.r, specular.g, specular.b, 1.0f));

			// Shininess factor:
			float shininess;
			memcpy(&shininess, data + position, sizeof(float));
			position += sizeof(float);
			
         tmpMaterial->setShininess(shininess);

			// Transparency factor:
			float alpha;
			memcpy(&alpha, data + position, sizeof(float));
			position += sizeof(float);
			
         tmpMaterial->setAlpha(alpha);

			m_materialMap[string(materialName)] = tmpMaterial;

         // UNUSED PROPERTIES
         // Diffuse texture filename, or [none] if not used:
         char textureName[OV_MAXNUMBEROFCHARS];
         strcpy(textureName, data + position);
         position += (unsigned int)strlen(textureName) + 1;

         if (textureName != "[none]") {
            Texture tex{ textureName };
            glGenTextures(1, tex.getTexId());
            tex.buildTexture();
            tmpMaterial->setTexture(tex);
         }

         // Normal map filename, or [none] if not used:
         char normalMapName[OV_MAXNUMBEROFCHARS];
         strcpy(normalMapName, data + position);
         position += (unsigned int)strlen(normalMapName) + 1;

         // Height map filename, or [none] if not used:
         char heightMapName[OV_MAXNUMBEROFCHARS];
         strcpy(heightMapName, data + position);
         position += (unsigned int)strlen(heightMapName) + 1;
      }
      break;

      ///////////
      default: //
         cout << "UNKNOWN]" << endl;
         cout << "ERROR: corrupted or bad data in file " << fileName << endl;
         fclose(dat);
         delete[] data;
         return nullptr;
      }

      // Release chunk memory:
      delete[] data;
   }

   // Done:
   fclose(dat);
   cout << "\nFile parsed" << endl;
  
	if (tmpNode == nullptr)
		cout << "No node found in ovo" << endl;

	return tmpNode;
}

/**
* Get the HyNgine instance, it is a singleton.
*
* @return the HyNgine's instance.
*/
HyNgine LIB_API *HyNgine::getInstance()
{
   if (instance == nullptr) {
      instance = new HyNgine();
   }      
   return instance;
}

/**
* Set the camera not under the root but under the given node.
* The camera in the scene graph has to be called 'Camera'.
* @param[in] node is the next camera's father.
*/
void HyNgine::setCameraToNode(Node * node)
{
	Camera *camera = ((Camera*)m_sceneGraph->findByName("Camera"));
	m_sceneGraph->removeChild(camera);
	node->addChild(camera);
	camera->setFather(node);
}

/**
* Create and return a new Camera.
* @param[in] nearPlane is the camera's near plane position.
* @param[in] farPlane is the camera's far plane position.
* @param[in] angle is the camera's vertical view angle.
*
* @return a new camera
*/
Camera LIB_API *HyNgine::generateCam(float nearPlane, float farPlane, float angle)
{
	return new Camera(nearPlane, farPlane, angle);
}

/**
* This method wrap glutSpecialFunc().
* @param[in] specialCallback is the glutSpecialFunc with his parameters.
*/
void LIB_API HyNgine::setSpecialCallback(void(*specialCallback)(int, int, int))
{
	glutSpecialFunc(specialCallback);
}

/**
* This method wrap glutKeybordFunc().
* @param[in] keyboardCallback is the glutKeybordFunc with his parameters.
*/
void LIB_API HyNgine::setKeyboardCallback(void(*keyboardCallback)(unsigned char, int, int))
{
   glutKeyboardFunc(keyboardCallback);
}

/**
* This method wrap glutIdleFunc().
* @param[in] idleCallback is the glutIdleFunc with his parameters.
*/
void LIB_API HyNgine::setIdleFunction(void(*idleCallback)(void))
{
   glutIdleFunc(idleCallback);
}

/**
* Remove the camera from a node and attach it to the root with given coordinates.
* The camera in the scene graph has to be called 'Camera'.
* @param[in] nodeToRemoveFrom the actual camera's father
* @ param[in] initialCoordinatesCamera are the coordinates of updated camera
*/
void HyNgine::setCameraToRoot(Node *nodeToRemoveFrom, vec3& initialCoordinatesCamera)
{
	Camera *camera = ((Camera*)m_sceneGraph->findByName("Camera"));
	m_sceneGraph->addChild(camera);
	camera->setFather(m_sceneGraph);
	nodeToRemoveFrom->removeChild(camera);
	((Camera*)camera)->setPosition(initialCoordinatesCamera);
}
