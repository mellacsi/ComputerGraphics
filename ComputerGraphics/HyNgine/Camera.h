/**
* @file		   Camera.h
* @brief		   Handle the camera
*
* @author		Kuhn, Mellace, Paoliello (C)
*/

#pragma once
///HyNgine
#include "Node.h"

class Camera : public Node
{
public:
	float getNearPlane() { return m_nearPlane; };
	float getFarPlane() { return m_farPlane; };	
   glm::mat4 getCameraMatrix() { return m_cameraMatrix; }; 
   glm::vec3 getCameraPositionVec() { return m_position; };
   float getFovy() { return m_fovy; };
   vec3 getOrientation() { return m_orientation; };
	void setNearPlane(float nearPlane) { m_nearPlane = nearPlane; };
	void setFarPlane(float farPlane) { m_farPlane = farPlane; };
	void setPosition(vec3 position) { m_position = position; };
   void setOrientation(vec3 &orientation) { m_orientation = normalize(orientation); };
	void setFovy(float angle) { m_fovy = angle; m_cameraMatrix[0][3]; };

	void render(mat4 *modifier = nullptr) override;

   Camera(float nearPlane, float farPlane, float angle);
   Camera(const Camera &camera);
   Camera& operator=(const Camera &camera);
	~Camera() {};

private:
   glm::mat4 m_cameraMatrix;
   glm::mat4 m_projection;
   float m_nearPlane;
   float m_farPlane;
   glm::vec3 m_position;
   glm::vec3 m_orientation;
   float m_fovy; /*!< Angle of view */
};

