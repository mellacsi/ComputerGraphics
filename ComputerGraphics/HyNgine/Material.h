/**
* @file		   Material.h
* @brief		   This class manages the materials of the Object.
*
* @author		Kuhn, Mellace, Paoliello (C)
*/
#pragma once
///HyNgine
#include "Object.h"
#include "Texture.h"

class Material : public Object
{
public:
   Texture & getTexture() { return m_texture; };
   void setTexture(Texture texture) { m_texture = texture; };
   void setTransparent(const bool &val) { m_transparent = val; };
   void setEmission(vec4 v) { m_emission = v; };
   void setAmbient(vec4 v) { m_ambient = v; };
   void setDiffuse(vec4 v) { m_diffuse = v; };
   void setSpecular(vec4 v) { m_specular = v; };
   void setShininess(float s) { m_shininess = s; };
   void setAlpha(float a);
   bool isTransparent() { return m_transparent; };
   void render(mat4 *modifier = nullptr) override;

   Material();
   Material(const Material &m);
   Material& operator=(const Material &m);
   ~Material();

private:
   Texture m_texture;
	bool m_transparent{ 0 };
   vec4 m_emission{ 0 };
   vec4 m_ambient{ 0 };
   vec4 m_diffuse{ 0 };
   vec4 m_specular{ 0 };
	float m_shininess;
	float m_alpha; /*!< Alpha channel for transparency */
};

