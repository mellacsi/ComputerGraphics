/**
* @file		   Node.h
* @brief		   Node is an element of the scene graph.
*
* @author		Kuhn, Mellace, Paoliello (C)
*/

#pragma once
///HyNgine
#include "Object.h"
///GLM
#include <vector>

#ifdef _WINDOWS 	
// Export API
// Specifies i/o linkage (VC++ spec)
#ifdef HYNGINE_EXPORTS
#define LIB_API __declspec(dllexport)
#else
#define LIB_API __declspec(dllimport)
#endif      	
#else // Under Linux
#define LIB_API  // Dummy declaration
#endif

class LIB_API Node : public Object
{
public:
	mat4& getContent() { return m_content; };
   int getNumberOfChildren() { return m_children.size(); };
   vector<Node*> getChildren() { return m_children; };
   Node *getFather() { return m_father; };
   mat4 getFinalMatrix();
   void setFinalMatrix(mat4 matrix) { m_finalMatrix = matrix; };
   void setContent(mat4 &content);
	void setFather(Node *node) { m_father = node; };
   void addChild(Node *node) { m_children.push_back(node);};
   void removeChild(Node *node);
   Object* findByName(string name) override;
   void updateFinalMatrix(mat4 *base);

   Node(Node* father) : m_father{ father } {};
   Node(const Node &n);
   Node& operator=(const Node &n);
   Node();


private:
	mat4 m_content; /*!< Position of the Object relative to his father */
   mat4 m_finalMatrix; /*!< World coordinate of the object, without the camera */
	vector<Node*> m_children;
	Node *m_father = nullptr;
};

