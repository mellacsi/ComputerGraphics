/**
* @file		   Texture.h
* @brief		   This class manages a Mesh's Texture.
*
* @author		Kuhn, Mellace, Paoliello (C)
*/

#pragma once
///HyNgine
#include "Object.h"

class Texture : public Object
{
public:
   void buildTexture();
   string getFileName() { return m_fileName; };
   unsigned int *getTexId() { return &m_texId; };
   void render(mat4 *modifier = nullptr) override;

   Texture(const string filename = "[none]") : m_fileName{ filename } {};
   Texture(const Texture &tex) : m_fileName{ tex.m_fileName }, m_texId{ tex.m_texId } {};
   Texture& operator=(const Texture &tex);
   ~Texture();

private:
   string m_fileName; /*!< Texture file's name or [none] */
   unsigned int m_texId = 0;
};

