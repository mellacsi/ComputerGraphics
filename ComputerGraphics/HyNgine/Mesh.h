/**
* @file		   Mesh.h
* @brief		   Mesh is a subtype of Node. Mesh is the main rendered entity made
* of vertices, it can have a material and a texture.
*
* @author		Kuhn, Mellace, Paoliello (C)
*/

#pragma once
/// HyNgine
#include "Node.h"
#include "Material.h"

/// C++
#include <iostream>
using namespace std;

class Mesh : public Node
{
public:
	Material& getMaterial() { return m_material; };
	float getSphereRadius() { return m_sphereRadius; };
	vector<vec3> getVertices() { return m_vertices; };
	void setMaterial(Material material) { m_material = material; };
	void setSphereRadius(float v) { m_sphereRadius = v; };
	void addVertex(vec3 v) { m_vertices.push_back(v); };
	void addNormal(vec4 v) { m_normals.push_back(v); };
	void addTextureCoords(vec2 v) { m_textureCoords.push_back(v); };
	void render(mat4 *modifier = nullptr) override;
	void sortTriangles();

   Mesh(Node *father) : Node(father) {};
   Mesh(const Mesh &m);
   Mesh& operator=(const Mesh &m);

private:
   Material m_material;
	float m_sphereRadius; /*!< For debug purpose */
	vector<vec3> m_vertices;
	vector<vec4> m_normals;
	vector<vec2> m_textureCoords;
};

