/// HyNgine
#include "Material.h"
#include "GL/freeglut.h"

/**
* Set alpha channel, ambient, diffuse and specular components.
* If the alpha is not 1 the object is considered transparent.
* @param[in] a is the alpha channel
*/
void Material::setAlpha(float a)
{
	m_alpha = a;
	m_transparent = a < 1.f ? true : false;
	m_ambient[3] = a;
	m_diffuse[3] = a;
	m_specular[3] = a;
}

/**
* Render the Material of the Mesh (if present)
* @param[in] modifier nullptr default value
*/
void Material::render(mat4 *modifier)
{
   glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, powf(2.0f, (float)m_shininess));
   glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, glm::value_ptr(m_ambient));
   glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, glm::value_ptr(m_diffuse));
   glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, glm::value_ptr(m_specular));
}

/**
* Constructor.
*/
Material::Material()
{
}

/*
* Copy contructor
*@param[in] m is the material to clone.
*/
Material::Material(const Material & m)
   : m_texture{ m.m_texture },
   m_transparent{ m.m_transparent },
   m_emission{ m.m_emission },
   m_ambient{ m.m_ambient },
   m_diffuse{ m.m_diffuse },
   m_specular{ m.m_specular },
   m_shininess{ m.m_shininess },
   m_alpha{ m.m_alpha }
{
}

/*
* Assignement operator
*@param[in] m is the material to assign.
*/
Material & Material::operator=(const Material & m)
{
   m_texture = m.m_texture;
   m_transparent = m.m_transparent;
   m_emission = m.m_emission;
   m_ambient = m.m_ambient;
   m_diffuse = m.m_diffuse;
   m_specular = m.m_specular;
   m_shininess = m.m_shininess;
   m_alpha = m.m_alpha;
   return *this;
}

/**
* Destructor.
*/
Material::~Material()
{
}
