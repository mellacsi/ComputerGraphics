/**
* @file		   HyNgine.h
* @brief		   A new and revolutionary graphics engine
*
* @author		Kuhn, Mellace, Paoliello (C)
*/
#pragma once

// Generic info
#define LIB_NAME      "HyNgine v0.0.1"  ///< Library credits
#define LIB_VERSION   10                  ///< Library version (divide by 10)

#ifdef _WINDOWS 	
   // Export API
   // Specifies i/o linkage (VC++ spec)
#ifdef HYNGINE_EXPORTS
#define LIB_API __declspec(dllexport)
#else
#define LIB_API __declspec(dllimport)
#endif      	
#else // Under Linux
#define LIB_API  // Dummy declaration
#endif

///HyNgine
#include "Object.h"
#include "Node.h"
#include "List.h"
#include "Camera.h"
#include "Triangle.h"
#include "Light.h"
#include <map>

/*! \def  GLU_KEY_
\brief GLUT API macro definitions -- the special key codes
*/
#define  GLUT_KEY_F1                        0x0001
#define  GLUT_KEY_F2                        0x0002
#define  GLUT_KEY_F3                        0x0003
#define  GLUT_KEY_F4                        0x0004
#define  GLUT_KEY_F5                        0x0005
#define  GLUT_KEY_F6                        0x0006
#define  GLUT_KEY_F7                        0x0007
#define  GLUT_KEY_F8                        0x0008
#define  GLUT_KEY_F9                        0x0009
#define  GLUT_KEY_F10                       0x000A
#define  GLUT_KEY_F11                       0x000B
#define  GLUT_KEY_F12                       0x000C
#define  GLUT_KEY_LEFT                      0x0064
#define  GLUT_KEY_UP                        0x0065
#define  GLUT_KEY_RIGHT                     0x0066
#define  GLUT_KEY_DOWN                      0x0067
#define  GLUT_KEY_PAGE_UP                   0x0068
#define  GLUT_KEY_PAGE_DOWN                 0x0069
#define  GLUT_KEY_HOME                      0x006A
#define  GLUT_KEY_END                       0x006B
#define  GLUT_KEY_INSERT                    0x006C

/*! \enum  ProjectionMode
\brief To set up projection mode you can use this Enumeration
*/
enum ProjectionMode {
   PERSPECTIVE,
   ORTHO
};

/**
* HyNgine main class
*/
class LIB_API HyNgine
{
public:
   static HyNgine *getInstance();
   glm::mat4 getPerspective() { return m_perspective; };
   glm::mat4 getOrtho() { return m_ortho; };
   Camera *getCamera() { return m_camera; };
   Node *getMovingObject() { return m_movingObject; };
   ProjectionMode getProjectionMode() { return m_projectionMode; };
   Node *getSceneGraph() { return m_sceneGraph; };
   List *getList() { return &m_list; };
   void setSceneGraph(Node *root) { m_sceneGraph = root; };
   void setPerspective(glm::mat4 perspective) { m_perspective = perspective; };
   void setOrtho(glm::mat4 ortho) { m_ortho = ortho; };
   void setCamera(Camera *camera) { m_camera = camera; };
   void setProjectionMode(ProjectionMode projectionMode) { m_projectionMode = projectionMode; };
   void setMovingObject(Node *movingObject) { m_movingObject = movingObject; };
   void setSpecialCallback(void(*specialCallback)(int, int, int));
   void setKeyboardCallback(void(*keyboardCallback)(unsigned char, int, int));
   void setIdleFunction(void(*idleCallback)(void));
   void setCameraToRoot(Node *nodeToRemoveFrom, vec3& initialCoordinatesCamera);
   void setCameraToNode(Node *node);
   Camera *generateCam(float nearPlane, float farPlane, float angle);
   Triangle *genTriangle(const float &edge);
   void destructTriangle(Triangle *triangle);
   Light *genLight();
   void initializeContext(int argc, char *argv[]);
   Node *loadMeshFromFile(string fileName);
   static void freeContext();
   void start();
   void setMenu(int argc, vector<string> argv);
   friend void displayCallback();
   void setReflections(const bool &value) { m_reflections = value; }
   static void freeScene(Node *root);

private:
   Node * m_sceneGraph = nullptr; /*!< Complete scene graph */
   List m_list; /*!< List of Object */
   int windowId;
   glm::mat4 m_perspective;
   glm::mat4 m_ortho;
   Camera *m_camera;
   ProjectionMode m_projectionMode = ProjectionMode::PERSPECTIVE;
   Node *m_movingObject; /*!< You can move the position of this Node */
   int m_idCounter = 0;
   bool m_reflections = false; /*!< By default value you can't see reflection */
   map<string, Material*> m_materialMap; /*!< Map of the Materials with associated names */
   int m_argc = 0;
   vector<string> m_argv;

   HyNgine() {};

   void loadChunk(FILE *dat, Node * node);
};