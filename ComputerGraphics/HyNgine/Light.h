/**
* @file		   Light.h
* @brief		   This class manages the light of the engine.
*
* @author		Kuhn, Mellace, Paoliello (C)
*/
#pragma once
///HyNgine
#include "Node.h"

/*! \enum ovLight
\brief Show the kind of a light.
*/
enum class ovLight
{
   /// Foundation types:
   SUBTYPE_OMNI = 0,
   SUBTYPE_DIRECTIONAL,
   SUBTYPE_SPOT,

   // Terminator:
   SUBTYPE_LAST,
};

class Light : public Node
{
public:
	glm::vec4 getLightAmbient() { return m_lightAmbient; };
	glm::vec4 getLightDiffuse() { return m_lightDiffuse; };
	glm::vec4 getLightSpecular() { return m_lightSpecular; };
	glm::vec3 getLightDirection() { return m_lightDirection; };
	ovLight getSubtype() { return m_subtype; };
	float getSpotAngle() { return m_spotAngle; };
	void setSubtype(unsigned char type) { m_subtype = (ovLight)(type); };
	void setColor(glm::vec4 c);
	void setLightAmbient(glm::vec4 ambient) { m_lightAmbient = ambient; };
	void setLightDiffuse(glm::vec4 diffuse) { m_lightDiffuse = diffuse; };
	void setLightSpecular(glm::vec4 specular) { m_lightSpecular = specular; };
	void setLightDirection(glm::vec3 direction) { m_lightDirection = direction; };
	void setSpotAngle(float angle) { m_spotAngle = angle; };

   //Turn on light
   void render(mat4 *modifier = nullptr) override;

   Light() = default;
   Light(const Light &l);
   Light& operator=(const Light &l);

private:
	glm::vec4 m_lightAmbient{ 1.f, 0.f, 0.f, 1.0f };
	glm::vec4 m_lightDiffuse{1.f, 0.f, 0.f, 1.0f};
	glm::vec4 m_lightSpecular{ 1.f, 0.f, 0.f, 1.0f };
	glm::vec3 m_lightDirection{ 0.0f, 0.0f, 0.0f };
	float m_lightCutoff = 180.0f;
	ovLight m_subtype;
	glm::vec4 m_color;
	float m_spotAngle;/*!< Field of aperture */
};

