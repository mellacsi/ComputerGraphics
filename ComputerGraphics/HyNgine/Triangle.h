/**
* @file		   Triangle.h
* @brief		   A simple class to generate a Triangle for debug purpose
*
* @author		Kuhn, Mellace, Paoliello (C)
*/

#pragma once
///HyNgine
#include "Mesh.h"

class Triangle : public Node
{
public:
	Triangle(const float edge) : m_edge{ edge } {}
   Triangle(const Triangle &t) : m_edge{ t.m_edge } {};
   Triangle& operator=(const Triangle &t) { m_edge = t.m_edge; return *this; };
   void render(mat4 *modifier = nullptr) override;

private:
	float m_edge; /*!< Edge of the triangle */
};

