/**
* @file		   List.h
* @brief		   This class is used to manage the engine's scene with a list.
*
* @author		Kuhn, Mellace, Paoliello (C)
*/
#pragma once
/// HyNgine
#include "Node.h"
/// C++
#include <iostream>
#include <vector>
using namespace std;

class List : public Object
{
public:
   void populate(Node *root);
   void clear();
   void sort();
   vector<Object*> getList() { return m_list; };

   void render(mat4 *modifier = nullptr) override;

   List() = default;
   List(const List &list) : m_list{ list.m_list.begin(), list.m_list.end() } {};
   List& operator=(const List &list) { m_list = vector<Object*>( list.m_list.begin(), list.m_list.end() ); return *this; };
   
private:
	vector<Object*> m_list;
};

