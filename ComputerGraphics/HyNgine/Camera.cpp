// HyNgine
#include "Camera.h"
#include "HyNgine.h"
#include <list>
// FreeGLUT
#include <GL/freeglut.h>

/**
* Activate and render the camera. Build the camera matrix based on eye
* and direction, taking into account the father of the node.
* @param[in] modifier nullptr or reflective matrix
*/
void Camera::render(mat4 *modifier) 
{
	list<mat4> contentMatrices;
	mat4 result = glm::mat4(1.f);
	vec3 eye;
	vec3 center;
	if (this->getFather()->getName() != "[root]") {

      result *= this->getFather()->getFinalMatrix();

		eye = result[3];
      eye += vec3(0.f, 15.f, 0.f) + m_orientation * -30.f; //offset
      center = eye + vec3(0.f, -0.3f, 0.f) + m_orientation;
	}
	else
	{
		eye = m_position;
      center = HyNgine::getInstance()->getMovingObject()->getContent()[3];
	}
	m_cameraMatrix = glm::lookAt(eye, center, vec3(0.0f, 1.0f, 0.0f));
	m_position = eye;
}

/**
* Constructor.
* @param[in] nearPlane is the near plane position.
* @param[in] farPlane is the far plane position.
* @param[in] angle is the vertical view angle.
*/
Camera::Camera(float nearPlane, float farPlane, float angle) :m_nearPlane{ nearPlane }, m_farPlane{ farPlane }, m_fovy{angle}
{
   glm::vec3 eye = m_position;
   glm:vec4 vec = HyNgine::getInstance()->getMovingObject()->getContent()[3];
   glm::vec3 center = glm::vec3(vec[0], vec[1], vec[2]);
   glm::vec3 up = glm::vec3(0.0f, 1.0f, 0.0f);
   m_cameraMatrix = glm::lookAt(eye, center, up);
}

/*
* Copy contructor
*@param[in] camera is the camera to clone.
*/
Camera::Camera(const Camera & camera)
   : m_nearPlane{ camera.m_nearPlane },
   m_farPlane{ camera.m_farPlane },
   m_fovy{ camera.m_fovy },
   m_cameraMatrix{ camera.m_cameraMatrix }
{
}


/*
* Assignement operator
*@param[in] camera is the camera to assign.
*/
Camera & Camera::operator=(const Camera & camera)
{
   m_nearPlane = camera.m_nearPlane;
   m_farPlane = camera.m_farPlane;
   m_fovy = camera.m_fovy;
   m_cameraMatrix = camera.m_cameraMatrix;
   return *this;
}
