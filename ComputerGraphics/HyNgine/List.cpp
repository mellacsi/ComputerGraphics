/// HyNgine
#include "List.h"
#include "Light.h"
#include "Mesh.h"
#include "HyNgine.h"
/// C++
#include <algorithm>

/**
* Populate the list with a scene graph.
* @param[in] root is the scene graph.
*/
void List::populate(Node * root)
{
   cout << root->getName() << endl;
	
   if (root->getNumberOfChildren() > 0)
      for (Node * n : root->getChildren())
         populate(n);
	else
	{
		if (dynamic_cast<Light*>(root) != nullptr)
			m_list.insert(m_list.begin(), root);
		else
			m_list.push_back((Object*)root);
	}
}

/**
* Clear the list.
*/
void List::clear()
{
	m_list.clear();
}

/**
* Sort list to have Light > Not transparent Object > Transparent Object
* In case of two transparent objects the further has the priority.
*/
void List::sort()
{
	std::sort(m_list.begin(), m_list.end(), [](void* a, void* b) {
		int priorityA, priorityB = 0;
		
		Mesh* meshA = dynamic_cast<Mesh*>((Object*)a);
		if (meshA == nullptr)
			priorityA = 1;
		else
		{
			if (meshA->getMaterial().isTransparent())
				priorityA = 3;
			else
				priorityA = 2;
		}
		if (dynamic_cast<Light*>((Object*)a) != nullptr)
			priorityA = 0;

		Mesh* meshB = dynamic_cast<Mesh*>((Object*)b);
		if (meshB == nullptr)
			priorityB = 1;
		else
		{
			if (meshB->getMaterial().isTransparent())
				priorityB = 3;
			else
				priorityB = 2;
		}
		if (dynamic_cast<Light*>((Object*)b) != nullptr)
			priorityB = 0;

		if(priorityA==priorityB && priorityA==3)
			return glm::length(vec3(meshA->getFinalMatrix()[3]) - HyNgine::getInstance()->getCamera()->getCameraPositionVec())
			> glm::length(vec3(meshB->getFinalMatrix()[3]) - HyNgine::getInstance()->getCamera()->getCameraPositionVec());
		return priorityA < priorityB;

      //Alternative method to sort the list

		/*if (dynamic_cast<Light*>((Object*)a) != nullptr)
			return true;
		Mesh* mesh = dynamic_cast<Mesh*>((Object*)a);
		if (mesh == nullptr)
			return false;
		// Case: a is an opaque mesh and b a light
		if (dynamic_cast<Light*>((Object*)b) != nullptr)
			return false;
		// Case: a is an opaque mesh and b is a general mesh
		if (!mesh->getMaterial().isTransparent())
			return true;
		Mesh* meshB= dynamic_cast<Mesh*>((Object*)b);
		if (!meshB->getMaterial().isTransparent())
			return false;
		// Case: a and b both transparent
		return glm::length(vec3(mesh->getFinalMatrix()[3]) - HyNgine::getInstance()->getCamera()->getCameraPositionVec())
			> glm::length(vec3(meshB->getFinalMatrix()[3]) - HyNgine::getInstance()->getCamera()->getCameraPositionVec());*/
	});
}

/**
* Call render method for each Object in the list.
* @param[in] modifier nullptr or reflective matrix
*/
void List::render(mat4 *modifier)
{
	for (Object *obj : m_list)
		obj->render(modifier);
}
