/**
* @file		   Object.h
* @brief		   This class is the father of all other objects(Node, Mesh,...). It contains main infos.
*
* @author		Kuhn, Mellace, Paoliello (C)
*/

#pragma once
/// GLM
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

/// C++
#include <iostream>
#include <string>
using namespace std;
using namespace glm;

class Object
{
public:
	string getName() { return m_name; };
	void setName(string name) { m_name = name; };
   virtual Object* findByName(string name) { return nullptr; };

   virtual void render(mat4 *modifier = nullptr) {};
   Object();
   Object(const Object &o) : m_name{ o.m_name } {};
   Object& operator=(const Object &o) { m_name = o.m_name; return *this; };

private:
   string m_name;
};

