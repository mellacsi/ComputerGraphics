/// HyNgine
#include "Mesh.h"
#include "GL/freeglut.h"
#include "HyNgine.h"
/// C++
#include <algorithm>

/**
* Render the Mesh with his Material (if present)
* and his Textures  (if present) in his right position.
* If transparent calculate the distance between each triangle and 
* the camera coordinates and sort them back to front.
* @param[in] modifier nullptr or reflective matrix
*/
void Mesh::render(mat4 *modifier)
{
	m_material.render(); /*!< Render material */

   bool hasTexture = m_material.getTexture().getFileName() != "[none]" ? true : false;

   if (hasTexture) {
      m_material.getTexture().render(); /*!< Render texture */
      glEnable(GL_TEXTURE_2D);
   }
	
   mat4 finalMat;
   if (modifier != nullptr)
      if (getMaterial().isTransparent())
         return;
      else
         finalMat = (*modifier) * getFinalMatrix(); /*!< Apply received modifier */
   else
      finalMat = getFinalMatrix();

   Camera *c = HyNgine::getInstance()->getCamera();

   // Render normal and reflected scene
   glLoadMatrixf(glm::value_ptr(c->getCameraMatrix() * finalMat));

   //glutWireSphere(m_sphereRadius, 8, 8); /*!< For debug purpose */
	if (m_material.isTransparent())
	{
		glDepthMask(GL_FALSE);
		sortTriangles(); /*!< Sort the triangle of the mesh for transparency */
	}
	else
		glDepthMask(GL_TRUE);

	// 2-pass for transparency rendering
	for (int pass = m_material.isTransparent() ? 0 : 1; pass < 2; pass++)
	{

		if (pass == 0)
			glCullFace(GL_FRONT);
		else
			glCullFace(GL_BACK);

		glBegin(GL_TRIANGLES);
      
		for (int k = 0; k < m_vertices.size(); k++)
		{
			vec3 v = m_vertices[k];
			vec4 n = m_normals[k];
			glNormal3f(n.x, n.y, n.z);
			if (hasTexture) {
				vec2 t = m_textureCoords[k];
				glTexCoord2f(t.x, t.y);
			}
			glVertex3f(v.x, v.y, v.z);
		}

		glEnd();
	}

   if (hasTexture)
      glDisable(GL_TEXTURE_2D);
	
}

/**
* Sort the triangles of the mesh for transparency purpose, from furthest
*  to closest to the camera.
*/
void Mesh::sortTriangles()
{
	vector<int> permutation;
	for (int i = 0; i < m_vertices.size()/3; i++)
		permutation.push_back(i);

	vector<vec3> eyeVertices;
	mat4 finalMatrix = HyNgine::getInstance()->getCamera()->getCameraMatrix()*getFinalMatrix();
	for (auto& v : m_vertices)
		eyeVertices.push_back(finalMatrix*vec4(v, 1.f));

   vec3 cameraPos(0.f); // From the camera perspective, the camera is at the origin
	std::sort(permutation.begin(),
      permutation.end(),
      [&eyeVertices,&cameraPos](size_t i, size_t j) {
      vec3 barycenterA = (eyeVertices[i*3] + eyeVertices[i*3+1] + eyeVertices[i*3+2]) / 3.f;
      vec3 barycenterB = (eyeVertices[j*3] + eyeVertices[j*3+1] + eyeVertices[j*3+2]) / 3.f;
		   return glm::length(cameraPos - barycenterA) > glm::length(cameraPos - barycenterB);
	});

	bool hasTexture = m_material.getTexture().getFileName() != "[none]" ? true : false;
	
	vector<vec3> tmpVertices = m_vertices;
	vector<vec4> tmpNormal = m_normals;
	vector<vec2> tmpTexture = m_textureCoords;

	for (int i = 0; i < permutation.size(); i++)
	{
		for (int k = 0; k < 3; k++)
		{
			m_vertices[i * 3 + k] = tmpVertices[permutation[i] * 3 + k];
			m_normals[i * 3 + k] = tmpNormal[permutation[i] * 3 + k];
			if (hasTexture)
				m_textureCoords[i * 3 + k] = tmpTexture[permutation[i] * 3 + k];
		}
	}
	// In loco alternative
	/*vector<vec3> tmpTriangle;
	vector<vec4> tmpNormal;
	vector<vec2> tmpTexture;
	for (size_t i = 0; i < permutation.size(); i++)
	{
		size_t j, k;
		if (i != permutation[i])
		{
			tmpTriangle.clear();
			tmpNormal.clear();
			if (hasTexture)
				tmpTexture.clear();
			for (int vertex = 0; vertex < 3; vertex++)
			{
				tmpTriangle.push_back(m_vertices[i * 3 + vertex]);
				tmpNormal.push_back(m_normals[i * 3 + vertex]);
				if (hasTexture)
					tmpTexture.push_back(m_textureCoords[i * 3 + vertex]);
			}
			k = i;
			while (i != (j = permutation[k]))
			{
				for (int vertex = 0; vertex < 3; vertex++)
				{
					m_vertices[k * 3 + vertex] = m_vertices[j * 3 + vertex];
					m_normals[k * 3 + vertex] = m_normals[j * 3 + vertex];
					if(hasTexture)
						m_textureCoords[k * 3 + vertex] = m_textureCoords[j * 3 + vertex];
				}
				
				permutation[k] = k;
				k = j;
			}
			for (int vertex = 0; vertex < 3; vertex++)
			{
				m_vertices[k * 3 + vertex] = tmpTriangle[vertex];
				m_normals[k * 3 + vertex] = tmpNormal[vertex];
				if (hasTexture)
					m_textureCoords[k * 3 + vertex] = tmpTexture[vertex];
			}
			permutation[k] = k;
		}
	}*/
}

/*
* Copy contructor
*@param[in] m is the mesh to clone.
*/
Mesh::Mesh(const Mesh & m)
   : m_material{ m.m_material },
   m_sphereRadius{ m.m_sphereRadius },
   m_vertices{ m.m_vertices },
   m_normals{ m.m_normals },
   m_textureCoords{ m.m_textureCoords }
{
}

/*
* Assignement operator
*@param[in] m is the mesh to assign.
*/
Mesh & Mesh::operator=(const Mesh & m)
{
   m_material = m.m_material;
   m_sphereRadius = m.m_sphereRadius;
   m_vertices = m.m_vertices;
   m_normals = m.m_normals;
   m_textureCoords = m.m_textureCoords;
   return *this;
}
