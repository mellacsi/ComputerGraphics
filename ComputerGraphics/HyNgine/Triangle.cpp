/// HyNgine
#include "HyNgine.h"
#include "Triangle.h"
#include "Camera.h"
/// FreeGLUT
#include <GL/freeglut.h>
/// GLM
#include <glm/gtc/type_ptr.hpp>

/**
* Renders a triangle with given edge size.
* @param[in] modifier is nullptr or reflective matrix
*/
void Triangle::render(mat4 *modifier)
{
	glNormal3f(0.0f, 0.f, 1.0f);

   mat4 finalMat;
   if (modifier != nullptr)
      finalMat = (*modifier) * getFinalMatrix();
   else
      finalMat = getFinalMatrix();

   Camera *c = HyNgine::getInstance()->getCamera();

	glMatrixMode(GL_MODELVIEW);
	glLoadMatrixf(glm::value_ptr(c->getCameraMatrix() * finalMat));

	glBegin(GL_TRIANGLE_STRIP);
	glVertex3f(-m_edge / 2.f, 0.f, -10.f);
	glVertex3f(m_edge / 2.f, 0.f, -10.f);
	glVertex3f(0.f, m_edge, -10.f);
	glEnd();
}