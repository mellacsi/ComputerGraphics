// HyNgine
#include "Texture.h"
// C++
#include <iostream>
#include <GL/freeglut.h>
// FreeImage
#include "FreeImage.h"

using namespace std;

/**
* Build a Texture and memorize it with FreeImage.
*/
void Texture::buildTexture()
{
   // Load image
   FIBITMAP *fbtm = FreeImage_Load(FreeImage_GetFileType(m_fileName.c_str(), 0), m_fileName.c_str());

   int intFormat = GL_RGB;
   GLenum extFormat = (unsigned int)GL_BGR_EXT;
   if (FreeImage_GetBPP(fbtm) == 32)
   {
      intFormat = GL_RGBA;
      extFormat =(unsigned int)GL_BGRA_EXT;
   }

   glBindTexture(GL_TEXTURE_2D, m_texId);

   glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
   glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
   glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
   glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR); // Without mipmap

   glTexImage2D(GL_TEXTURE_2D, 0, intFormat, FreeImage_GetWidth(fbtm), FreeImage_GetHeight(fbtm), 0, extFormat, GL_UNSIGNED_BYTE, (void *)FreeImage_GetBits(fbtm));

   FreeImage_Unload(fbtm);
}

/**
* Load current texture.
* @param[in] modifier nullptr or reflective matrix
*/
void Texture::render(mat4 *modifier)
{
   glBindTexture(GL_TEXTURE_2D, m_texId);
}

/**
* Destructor.
*/
Texture::~Texture()
{
}


/*
* Assignement operator
*@param[in] text is the text to assign.
*/
Texture & Texture::operator=(const Texture & tex)
{
   m_fileName = tex.m_fileName;
   m_texId = tex.m_texId;
   return *this;
}