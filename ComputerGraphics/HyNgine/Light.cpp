/// HyNgine
#include "HyNgine.h"
#include "Light.h"
#include "Camera.h"
/// FreeGLUT
#include <GL/freeglut.h>
/// GLM
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

/**
* Set the color to a given value. 
* @param[in] c contains light's ambient, diffuse and specular component
*/
void Light::setColor(glm::vec4 c)
{
	m_color = c;
	setLightAmbient(c);
	setLightDiffuse(c);
	setLightSpecular(c);
}

/**
* Give values to light and render it.
* Can render a omni light or a spot light.
* @param[in] modifier nullptr or reflective matrix
*/
void Light::render(mat4 *modifier)
{
      mat4 finalMat;
      if (modifier != nullptr)
         return;
      finalMat = getFinalMatrix();
      vec4 pos{ finalMat[3] };

      vec3 cameraDirection = HyNgine::getInstance()->getCamera()->getOrientation();

      vec3 finalDirection = 7.f * cameraDirection;
      finalDirection.y = -20.f;
      
      glLoadMatrixf(glm::value_ptr(HyNgine::getInstance()->getCamera()->getCameraMatrix() * finalMat));


      switch (m_subtype)
      {
      case ovLight::SUBTYPE_OMNI:
         glLightfv(GL_LIGHT0, GL_POSITION, glm::value_ptr(vec4(0.f, 0.f, 0.f, 1.f)));
         glLightfv(GL_LIGHT0, GL_AMBIENT, glm::value_ptr(getLightAmbient()));
         glLightfv(GL_LIGHT0, GL_DIFFUSE, glm::value_ptr(getLightDiffuse()));
         glLightfv(GL_LIGHT0, GL_SPECULAR, glm::value_ptr(getLightSpecular()));
         glLightf(GL_LIGHT0, GL_SPOT_CUTOFF, m_lightCutoff);
         break;
      case ovLight::SUBTYPE_SPOT:
         glLightfv(GL_LIGHT1, GL_POSITION, glm::value_ptr(vec4(0.f, 0.f, 0.f, 1.f)));
         glLightfv(GL_LIGHT1, GL_SPOT_DIRECTION, glm::value_ptr(finalDirection));
         glLightfv(GL_LIGHT1, GL_AMBIENT, glm::value_ptr(getLightAmbient()));
         glLightfv(GL_LIGHT1, GL_DIFFUSE, glm::value_ptr(getLightDiffuse()));
         glLightfv(GL_LIGHT1, GL_SPECULAR, glm::value_ptr(getLightSpecular()));
         glLightf(GL_LIGHT1, GL_SPOT_CUTOFF, m_spotAngle);
         break;
      }
}

/*
* Copy contructor
*@param[in] l is the l to clone.
*/
Light::Light(const Light & l)
   : m_lightAmbient{ l.m_lightAmbient },
   m_lightDiffuse{ l.m_lightDiffuse },
   m_lightSpecular{ l.m_lightSpecular },
   m_lightDirection{ l.m_lightDirection },
   m_lightCutoff{ l.m_lightCutoff },
   m_subtype{ l.m_subtype },
   m_color{ l.m_color },
   m_spotAngle{ l.m_spotAngle }
{
}


/*
* Assignement operator
*@param[in] l is the light to assign.
*/
Light & Light::operator=(const Light & l)
{
   m_lightAmbient = l.m_lightAmbient;
   m_lightDiffuse = l.m_lightDiffuse;
   m_lightSpecular = l.m_lightSpecular;
   m_lightDirection = l.m_lightDirection;
   m_lightCutoff = l.m_lightCutoff;
   m_subtype = l.m_subtype;
   m_color = l.m_color;
   m_spotAngle = l.m_spotAngle;
   return *this;
}

