#include "HyNgine.h"
/**
* Global engine variable
*/
HyNgine *engine; /*!< Engine instance */
vec3 initialCoordinatesCamera; /*!< Camera coordinates */
vec4 helicopterDirection(0.f, 0.f, -1.f, 0.f); /*!< Initial helicopter direction */
vec4 initialPosition; /* Initial helicopter position */
bool gameover = false;

/**
* This callback is invoked each time a special keyboard key is pressed.
* @param[in] key key pressed id
* @param[in] mouseX mouse X coordinate
* @param[in] mouseY mouse Y coordinate
*
*@return nothing
*/
void specialCallback(int key, int mouseX, int mouseY)
{
   if (gameover)
      return;

   //std::cout << "[special key pressed]" << std::endl;
   float speed = 1.f;
   float rotation = 2.f;
	switch (key)
	{
	case GLUT_KEY_UP:
      {
		mat4 firstMat = translate(mat4(1.f), vec3(helicopterDirection));
      mat4 secondMat = engine->getMovingObject()->getContent();
      mat4 finalMat = firstMat * secondMat;
		engine->getMovingObject()->setContent(finalMat);
		break;
      }

   case GLUT_KEY_DOWN:
      {
		mat4 firstMat = translate(mat4(1.f), vec3(-helicopterDirection));
      mat4 secondMat = engine->getMovingObject()->getContent();
      mat4 finalMat = firstMat * secondMat;
		engine->getMovingObject()->setContent(finalMat);
		break;
      }

	case GLUT_KEY_LEFT:
      {
      helicopterDirection = rotate(mat4(1.f), radians(rotation), vec3(0.f, 1.f, 0.f)) * helicopterDirection;
      mat4 mat = rotate(engine->getMovingObject()->getContent(), radians(rotation), vec3(0.f, 1.f, 0.f));
      engine->getMovingObject()->setContent(mat);
      vec3 helicopterDirectionVec3 = vec3(helicopterDirection);
      engine->getCamera()->setOrientation(helicopterDirectionVec3);
		break;
      }

	case GLUT_KEY_RIGHT:
	   {
      helicopterDirection = rotate(mat4(1.f), radians(-rotation), vec3(0.f, 1.f, 0.f)) * helicopterDirection;
      mat4 mat = rotate(engine->getMovingObject()->getContent(), radians(-rotation), vec3(0.f, 1.f, 0.f));
      engine->getMovingObject()->setContent(mat);
      vec3 helicopterDirectionVec3 = vec3(helicopterDirection);
      engine->getCamera()->setOrientation(helicopterDirectionVec3);
      break;
	   }
	}
}

/**
* Determines if the given position is on one of the platform. This is hard-coded for simplicity.
*
* @param[in] pos the position to check
*
* @return true if the position is on either one of the platforms
*/
bool isOnPlatform(const vec4 &pos)
{
	// Check if on starting platform
	if (pos.x <= 39.f && pos.x >= 23.5f && pos.z >= 15.f && pos.z <= 30.f)
		return true;

	// Check if on ending platform
	if (pos.x <= -5.f && pos.x >= -20.f && pos.z >= -34.5f && pos.z <= -29.5f)
		return true;

	return false;
}

/**
* This callback is invoked each time a standard keyboard key is pressed.
* @param key key pressed id
* @param mouseX mouse X coordinate
* @param mouseY mouse Y coordinate
*
*@return nothing
*/
void keyboardCallback(unsigned char key, int mouseX, int mouseY)
{
   //std::cout << "[std key pressed]" << std::endl;
   float speed = 1.f;
	Node *camera;
	Node* helicopter;
   switch (key)
   {
   case 'c':
		camera = (Camera*)((Node*)engine->getSceneGraph()->findByName("Camera"));

		if (camera->getFather()->getName() == "[root]") /*!< Camera is global*/
		{
			helicopter = ((Node*)engine->getSceneGraph()->findByName("Helicopter"));
         if (helicopter != nullptr)
			   engine->setCameraToNode(helicopter);
		}
		else /*!< Camera is on helicopter */
		{
			helicopter = ((Node*)engine->getSceneGraph()->findByName("Helicopter"));
         if (helicopter != nullptr)
			   engine->setCameraToRoot(helicopter, initialCoordinatesCamera);
		}
		break;
   case ' ': /*!< SPACE */
        {
      if (gameover)
         break;

        glm::mat4 mat = glm::translate(glm::mat4(1.f), glm::vec3(0, speed, 0)) * engine->getMovingObject()->getContent();
        engine->getMovingObject()->setContent(mat);
        break;
        }
   case 'd':
       {
		mat4 &heliMat = engine->getMovingObject()->getContent();
      if (gameover || (isOnPlatform(heliMat[3]) && heliMat[3].y<11.5f))
         break;
       glm::mat4 mat = glm::translate(glm::mat4(1.f), glm::vec3(0, -speed, 0)) * engine->getMovingObject()->getContent();
       engine->getMovingObject()->setContent(mat);
       break;
       }
   case 13: /*!< ENTER */
      {
       glm::mat4 mat = glm::translate(glm::mat4(1.f), vec3(initialPosition));
       engine->getMovingObject()->setContent(mat);
       vec3 orientation = vec3(0.f, 0.f, -1.f);
       engine->getCamera()->setOrientation(orientation);
       helicopterDirection = vec4(0.f, 0.f, -1.f, 0.f);
       vector<string> text{ "MOVEMENTS:", "      UP, DOWN, LEFT, RIGHT", "PROPULSION: SPACEBAR", "CAMERA SWITCH: C"};
       engine->setMenu(4, text);
       gameover = false;
       break;
      }
   }
}



/**
* This callback manage the game logic.
*
*@return nothing
*/
void idleCallback()
{
   if (gameover)
      return;

   // Handle gravity
   Mesh *heli = (Mesh*) engine->getMovingObject();
   mat4 heliMat = heli->getContent();
	if (!isOnPlatform(heliMat[3]) || heliMat[3].y > 10.5f)
    {
        vec3 vec = vec3(0.f, -0.004f, 0.f);
        mat4 translation = translate(heliMat, vec);
		heli->setContent(translation);
    }

   // Handle rotations of helicopter blades
   Mesh *topBlade = dynamic_cast<Mesh*>(engine->getSceneGraph()->findByName("TopBlade"));
   Mesh *sideBlade = dynamic_cast<Mesh*>(engine->getSceneGraph()->findByName("SideBlade"));
   if (topBlade != nullptr)
   {
      mat4 rotation = glm::rotate(topBlade->getContent(), radians(9.f), vec3(0.f, 1.f, 0.f));
      topBlade->setContent(rotation);
   }
   if (sideBlade != nullptr)
   {
       mat4 rotation = glm::rotate(sideBlade->getContent(), radians(9.f), vec3(0.f, 1.f, 0.f));
       sideBlade->setContent(rotation);
   }

   // Handle collisions
	vector<Object*> list = engine->getList()->getList();
	Mesh *movingObject = dynamic_cast<Mesh*>(engine->getSceneGraph()->findByName("Body"));
	if (isOnPlatform(movingObject->getFinalMatrix()[3]))
		return;

	for (Object* o : list)
   {
		Node* node = dynamic_cast<Node*>(o);
		if (node == nullptr || node->getName()=="Body")
			continue;

		bool isAChild = false;
		for(Node* runner = node; runner->getName()!="[root]"; runner=runner->getFather())
			if (runner->getName() == "Helicopter")
			{
				isAChild = true;
				break;
			}
		if (isAChild)
			continue;
		Mesh *mesh = dynamic_cast<Mesh*>(node);
		if (mesh == nullptr)
			continue;
      vec3 meshPivot = mesh->getFinalMatrix()[3];
      vec3 movingObjectFinalPosition = movingObject->getFinalMatrix()[3];
		for (auto &v : mesh->getVertices())
         if (movingObject->getSphereRadius() > glm::length(v + meshPivot - movingObjectFinalPosition))
         {
            vector<string> text{ "GAME OVER", "Click enter to reset the game!" };
            engine->setMenu(2, text);
            gameover = true;
         }
	}
}

int main(int argc, char *argv[])
{
	engine = HyNgine::getInstance();
	engine->initializeContext(argc, argv);

   Node* root = engine->loadMeshFromFile("finalScene.ovo");
   Node *moving = (Node *)root->findByName("Helicopter");
   if (moving != nullptr)
   {
      engine->setMovingObject(moving);
      initialPosition = moving->getContent()[3];
   }
   else
      cout << "moving object not found" << endl;

   Camera *camera = engine->generateCam(1., 300.f, 45.f);
	initialCoordinatesCamera = vec3(7.0f, 70.0f, 87.0f);
	camera->setPosition(initialCoordinatesCamera);
	camera->setName("Camera");

   vector<string> text{ "MOVEMENTS:", "      UP, DOWN, LEFT, RIGHT", "PROPULSION: SPACEBAR", "CAMERA SWITCH: C" };
   engine->setMenu(4, text);

	engine->setProjectionMode(ProjectionMode::PERSPECTIVE);
	engine->setCamera(camera);
   root->addChild(camera);
	camera->setFather(root);
   engine->setSceneGraph(root);
	engine->setSpecialCallback(specialCallback);
   engine->setKeyboardCallback(keyboardCallback);
	engine->setIdleFunction(idleCallback);

   engine->setReflections(true);

   engine->start();

   HyNgine::freeContext();

	return 0;
}
